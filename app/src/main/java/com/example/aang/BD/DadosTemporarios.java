package com.example.aang.BD;

import android.content.Intent;

import com.example.aang.Domain.Script;

public class DadosTemporarios {
    public static boolean precisaSerExecutado=false;
    public static Intent intent = null;

    public static void setIntent(Intent i){
        intent = i;
    }
    public static Intent getIntent(){
        return intent;
    }
    public static void setExecucao(Boolean bool){
        precisaSerExecutado= bool;
    }

    public static Boolean getExecucao(){
        return precisaSerExecutado;
    }

}
