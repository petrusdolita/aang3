package com.example.aang.BD;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.aang.Domain.Categoria;
import com.example.aang.Domain.Script;

import java.util.ArrayList;

public class BancoBD extends SQLiteOpenHelper {

    public BancoBD(Context context){
        super(context,"Automacao",null,1);
    }

    @Override
    public void onCreate(SQLiteDatabase db){
        db.execSQL("Create table categoria(codigo integer primary key autoincrement, categoria varchar(60))");
        db.execSQL("Create table script(id integer primary key autoincrement, foto text, texto text, titulo varchar(60), idcategoria integer, " +
                "foreign key (idCategoria) references categoria(codigo))");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        db.execSQL("drop table categoria");
        db.execSQL("drop table script");
        onCreate(db);
    }

    public ContentValues obterDadosCategoria(Categoria categoria) {
        ContentValues values = new ContentValues();

        values.put("categoria", categoria.getCategoria());

        return values;
    }

    public ContentValues obterDadosScript(Script script) {
        ContentValues values = new ContentValues();

        values.put("texto", script.getTexto());
        values.put("titulo", script.getTitulo());
        values.put("idcategoria", script.getIdCategoria());
        values.put("foto",script.getFoto());

        return values;
    }

    private SQLiteDatabase db(){
        return getWritableDatabase();
    }

    public void adicionarCategoria(Categoria categoria) {

        ContentValues values = obterDadosCategoria(categoria);

        if(listarCategoriaExistente(categoria)=="")
            db().insert("categoria", null, values);
    }
    public void adicionarScript(Script script) {
        ContentValues values = obterDadosScript(script);

        db().insert("script", null, values);
    }

    public void atualizarCategoria(Categoria categoria) {
        ContentValues values = obterDadosCategoria(categoria);

        getWritableDatabase().update("categoria", values, "codigo='"+categoria.getIdCategoria()+"'", null);
    }
    public void atualizarScript(Script script) {
        ContentValues values = obterDadosScript(script);

        getWritableDatabase().update("script", values, "id='"+script.getId()+"'", null);
    }

    public void deletarCategoria(Categoria categoria) {

        getWritableDatabase().delete("categoria", "codigo='"+categoria.getIdCategoria()+"'", null);
    }

    public void deletarScript(Script script) {

        getWritableDatabase().delete("script", "id='"+script.getId()+"'", null);
    }

    public ArrayList<Categoria> listarCategorias() {
        ArrayList<Categoria> categorias = new ArrayList<>();

        try {
            Cursor cursor = getWritableDatabase().rawQuery("SELECT * FROM categoria", null);

            while(cursor.moveToNext()) {
                categorias.add(new Categoria(
                        cursor.getInt(cursor.getColumnIndex("codigo")),
                        cursor.getString(cursor.getColumnIndex("categoria"))
                ));
            }
        }
        catch(SQLiteException e) { }

        return categorias;
    }
    public ArrayList<Script> listarScripts() {
        ArrayList<Script> scripts = new ArrayList<>();

        try {
            Cursor cursor = getWritableDatabase().rawQuery("SELECT * FROM script", null);

            while(cursor.moveToNext()) {
                scripts.add(new Script(
                        cursor.getInt(cursor.getColumnIndex("id")),
                        cursor.getString(cursor.getColumnIndex("texto")),
                        cursor.getString(cursor.getColumnIndex("titulo")),
                        cursor.getInt(cursor.getColumnIndex("idcategoria")),
                        cursor.getString(cursor.getColumnIndex("foto"))
                ));
            }
        }
        catch(SQLiteException e) { }

        return scripts;
    }

    public String listarCategoria(Script script) {
        Categoria categorias;

        try {
            Cursor cursor = getWritableDatabase().rawQuery("SELECT * FROM categoria where codigo='"+script.getIdCategoria()+"'", null);

            while(cursor.moveToNext()) {
                return   cursor.getString(cursor.getColumnIndex("categoria"));
            }
        }
        catch(SQLiteException e) { }

        return "";
    }

    public String listarCategoriaExistente(Categoria categoria) {

        try {
            Cursor cursor = getWritableDatabase().rawQuery("SELECT * FROM categoria where categoria='"+categoria.getCategoria()+"'", null);

            while(cursor.moveToNext()) {
                return   cursor.getString(cursor.getColumnIndex("categoria"));
            }
        }
        catch(SQLiteException e) { }

        return "";
    }
}
