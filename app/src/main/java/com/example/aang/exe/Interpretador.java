package com.example.aang.exe;

import android.content.Context;
import android.widget.Toast;

import com.example.aang.ui.MainActivity;

import java.io.DataOutputStream;

public class Interpretador {

    private String textoo;

    //Limpando espaços, quebras de linha e comentários



    public Interpretador(String textinho){
        textoo= textinho;
    }


    private static String limpar(String input){

        String output = "";
        boolean dentroDeString=false;
        boolean dentroDeComentario=false;

        for(int i=0;i<input.length();i++){

            while((dentroDeComentario)||(!dentroDeString&&(input.charAt(i)==' '||input.charAt(i)=='\n'))){
                if(input.charAt(i)=='\\')
                    dentroDeComentario=false;

                if(++i==input.length())
                    break;
            }
            if(i==input.length())
                break;

            if(input.charAt(i)=='/'){
                dentroDeComentario=true;

            }else{
                if(input.charAt(i)=='"'&&!dentroDeComentario){
                    dentroDeString=!dentroDeString;

                }

                output= output+input.charAt(i);
            }
        }
        return output;
    }

    public static int identificarCategoria(String input){
        String[] output = new String[2];
        input= limpar(input);
        String categoria= "";
        int position=-1, codCategoria;

        /*
           -1 -> Erro
            0 -> Imediatamente
            1 -> Carregador
            2 -> Conectado à rede

        */
        for(int i=0;i<input.length();i++){
            if(input.charAt(i)=='!')
            {
                position = i;

                break;
            }
        }
        if(position==-1)
            return -1;

        categoria = input.substring(0,position);

        if(categoria.equals("IMEDIATAMENTE")){
            codCategoria=1;
        } else if(categoria.equals("CARREGADOR")){
            codCategoria=2;
        } else if(categoria.equals("CONECTADO")){
            codCategoria=3;
        } else
            codCategoria= -1;

        return codCategoria;
    }


    public static int interpretar(String input, int vezes){

        input=limpar(input);

        int auxi=0, auxf=-1, auxli=0, auxlf=-1;



        boolean dentroDeString=false, emComando=false;

        for(;vezes>0;vezes--){
            auxi=0;
            auxf=-1;
            auxli=0;
            auxlf=-1;
            int inLacos=0;




            for(int i=0;i<input.length();i++){
                System.out.println(i+" ___ "+input.charAt(i));
                if(inLacos>0){
                    switch(input.charAt(i)){
                        case '{':
                            inLacos++;
                            break;
                        case '}':
                            inLacos--;
                            break;

                    }
                    if(inLacos==0){
                        auxlf=i;
                        int inicioDoParametro=0,fimDoParametro=0;

                        for(int j=auxli;true;j--){
                            if(input.charAt(j)==')'){
                                fimDoParametro=j;
                            }else if(input.charAt(j)=='('){
                                inicioDoParametro=j+1;
                                break;
                            }

                        }



                        int repeticoes=Integer.parseInt(input.substring(inicioDoParametro,fimDoParametro));
                        auxi = i+1;
                        interpretar(input.substring(auxli,auxlf),repeticoes);
                    }
                }else switch(input.charAt(i)){
                    case '!':
                        auxi=i+1;
                        break;
                    case ')':
                        auxf=i+1;
                        if(input.charAt(i)!='{'){
                            executarFuncao(input.substring(auxi,auxf));
                            if(input.charAt(i)==';')
                                auxi=i+3;

                            else
                                auxi= i+2;

                        }
                        break;

                    case '{':
                        inLacos++;
                        auxli=i+1;
                        break;

                }

            }


        }
        return 0;
    }


    public static int executarFuncao(String funcao){
        /*
           -1 -> Nenhuma
            0 -> repetir
            1 -> clicar
            2 -> esperar

            3 -> digitar
        */
        String tipoFuncao= "";
        final String [] parametros = new String[6];
        parametros[0]="";
        parametros[1]="";
        parametros[2]="";
        parametros[3]="";
        parametros[4]="";
        parametros[5]="";
        System.out.println("-"+funcao);
        int auxi=0,virgulas=0;

        for(int i=0;i<funcao.length();i++){

            switch(funcao.charAt(i)){
                case '(':
                    tipoFuncao= funcao.substring(0, i);
                    auxi=i+1;
                    break;
                case ',':


                    parametros[virgulas]= funcao.substring(auxi,i);
                    auxi= i+1;
                    System.out.println(parametros[virgulas]+" - "+virgulas);
                    virgulas++;
                    break;

                case ')':
                    parametros[virgulas]= funcao.substring(auxi,i);
                    System.out.println(parametros[virgulas]+" - "+virgulas);
                    break;

            }
        }

        if(tipoFuncao.equals("clicar")){

            Thread novo = new Thread(){
                @Override
                public void run() {

                    try {


                        Process p= Runtime.getRuntime().exec("su");
                        DataOutputStream dos = new DataOutputStream(p.getOutputStream());
                        dos.writeBytes("input tap "+parametros[0]+ " "+parametros[1]+"\n");
                        dos.writeBytes("exit\n");
                        dos.flush();
                        dos.close();
                        p.waitFor();




                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

            };
            novo.start();

        }
        else if(tipoFuncao.equals("esperar")){
            try{

                Thread.sleep(Integer.parseInt(parametros[0]));

            }catch (InterruptedException e) {
                e.printStackTrace();
            }

        }
        else if(tipoFuncao.equals("digitar")){
            System.out.println("Ainda não implementado");

        }
        else if(tipoFuncao.equals("arrastar")){
            Thread novo = new Thread(){
                @Override
                public void run() {

                    try {

                        Process p= Runtime.getRuntime().exec("su");
                        DataOutputStream dos = new DataOutputStream(p.getOutputStream());
                        dos.writeBytes("input swipe "+parametros[0]+ " "+parametros[1]+" "+parametros[2]+ " "+parametros[3]+ " "+parametros[4]+ "\n");
                        dos.writeBytes("exit\n");
                        dos.flush();
                        dos.close();
                        p.waitFor();


                        //Runtime.getRuntime().exec("input swipe "+parametros[0]+ " "+parametros[1]+" "+parametros[2]+ " "+parametros[3]+ " "+parametros[4]+ " ");

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

            };
            novo.start();
        }



        return 0;
    }


}
