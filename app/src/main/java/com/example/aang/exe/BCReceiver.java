package com.example.aang.exe;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Build;
import android.widget.Toast;

import com.example.aang.BD.DadosTemporarios;

public class BCReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent){
        if(Intent.ACTION_POWER_CONNECTED.equals(intent.getAction())){
            Toast.makeText(context, "Carregando!", Toast.LENGTH_SHORT).show();
            if(DadosTemporarios.getExecucao()) {
                Intent i = DadosTemporarios.getIntent();
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    context.startForegroundService(i);
                }else{
                    context.startService(i);
                }



            }
            context.unregisterReceiver(this);
        }

        else if(ConnectivityManager.CONNECTIVITY_ACTION.equals(intent.getAction())){
            boolean noConnectivity = intent.getBooleanExtra(
                    ConnectivityManager.EXTRA_NO_CONNECTIVITY, false
            );

            if(!noConnectivity){
                Toast.makeText(context, "Conectado à Internet!", Toast.LENGTH_SHORT).show();
                if(DadosTemporarios.getExecucao()) {
                    Intent i = DadosTemporarios.getIntent();
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        context.startForegroundService(i);
                    }else{
                        context.startService(i);
                    }



                }
                context.unregisterReceiver(this);
            }


        }

    }

}
