package com.example.aang.exe;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;
import com.example.aang.Domain.Script;
import com.example.aang.R;
import com.example.aang.ui.MainActivity;

import static com.example.aang.exe.App.CHANNEL_ID;
import static com.example.aang.exe.Interpretador.interpretar;
public class Executor extends Service {

    private Notification notif;



    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId){


            if (intent.hasExtra("script")) {
                System.out.println("Começando");

                String input = intent.getStringExtra("inputExtra");

                Intent notificationIntent = new Intent(this, MainActivity.class);
                PendingIntent pendingIntent = PendingIntent.getActivity(this,
                        0, notificationIntent, 0);

                Notification notification = new NotificationCompat.Builder(this, CHANNEL_ID)
                        .setContentTitle("Aang")
                        .setContentText("Executando script")
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setContentIntent(pendingIntent)
                        .build();

                startForeground(1, notification);

                Script script = (Script) intent.getSerializableExtra("script");



                interpretar(script.getTexto(),1);



                NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                mNotificationManager.cancel(1);
                System.out.println("Terminando");
            }

            Intent a = new Intent(this, Executor.class);
            stopService(a);

        return START_NOT_STICKY;
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }




}
