package com.example.aang.ui;

import android.os.Bundle;

import com.example.aang.R;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;

import android.view.View;
import android.widget.TextView;

public class TutorialActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tutorial_activity);
        TextView textinho = findViewById(R.id.textinho);

        textinho.setText("REQUISITOS:\n" +
                "\n" +
                "  -Versão do Android a partir da 4.4\n" +
                "  -Dispositivo com Root\n" +
                "\n" +
                "OBTENÇÃO DE COORDENADAS\n" +
                "\n" +
                "  Esse método irá fazer com que apareça uma indicação das posições absolutas dos cliques na parte superior do telefone, para usar tal informação no script.\n" +
                "\n" +
                "    Configurações -> Sobre o telefone -> Clique em \"Número da versão\" 7x para desbloquear as opções de desenvolvedor.\n" +
                "    <- Sobre o telefone <- Configurações.\n" +
                "    Configurações -> Opções de desenvolvedor -> Ative \"Localização do ponteiro\".\n" +
                "\n" +
                "SCRIPT:\n" +
                "\n" +
                "  Estrutura:\n" +
                "\n" +
                "    ';' - Separa sentenças.\n" +
                "    '{' e '}' - Início e fim de laço de repetição;\n" +
                "    '/' e '\\' - Início e fim de comentários;\n" +
                "    '!' - Finaliza a declaração da categoria de inicialização do script;\n" +
                "\n" +
                "  Funções:\n" +
                "\n" +
                "    EVENTO! - Irá executar o script apenas quando o evento \"EVENTO\" ocorrer;\n" +
                "    repetir (n){ /* Ações */ }  - Irá repetir o conteúdo n vezes;\n" +
                "    clicar (x,y); - Irá clicar na posição (x,y);\n" +
                "    arrastar (x,y,x1,y1,t); - Irá arrastar da posição (x,y) até a posição (x1,y1) durantes t milissegundos;\n" +
                "    esperar (int n); - Irá esperar por n milissegundos até executar o próximo comando;\n" +
                "    Obs.: 0<=n<=4294967295;\n" +
                "          x e y variam de 0 até, respectivamente, a largura e altura em pixels máximas -1 da tela do teu dispositivo;\n" +
                "          O \"EVENTO\" pode ser:\n" +
                "            \"IMEDIATAMENTE\" - Inicia quando tocas o botão de executar;\n" +
                "            \"CARREGADOR\" - Inicia quando conectas o carregador no dispostivo;\n" +
                "            \"CONECTADO\" - Inicia no momento que o telefone estiver com conexão à internet.\n" +
                "\n" +
                "\n" +
                "\n" +
                "\n" +
                "  Exemplo:\n" +
                "\n" +
                "      IMEDIATAMENTE! /Irá iniciar imediatamente\\\n" +
                "      repetir (40){ /Irá repetir 40 vezes o código a seguir\\\n" +
                "\n" +
                "        clicar (100, 200); /Irá clicar na posição (100,200) da tela\\\n" +
                "        esperar (2000); /Irá esperar por dois segundos para executar o próximo comando\\\n" +
                "        arrastar (100,100,200,200,1000); /Irá arrastar da posição (100,100) até a (200,200) em 1 segundo\\\n" +
                "\n" +
                "      }\n");

    }

}
