    package com.example.aang.ui;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.aang.BD.BancoBD;
import com.example.aang.Domain.Script;
import com.example.aang.R;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.List;

import static com.example.aang.ui.MainActivity.dao;

public  class AdapterScript extends BaseAdapter {

    private List<Script> listaScripts;
    private Context context;

    public AdapterScript(List<Script> listaScripts, Context context) {
        this.listaScripts = listaScripts;
        this.context = context;
    }

    @Override
    public int getCount() {
        return listaScripts.size();
    }

    @Override
    public Script getItem(int position) {
        return listaScripts.get(position);
    }

    @Override
    public long getItemId(int position) {
        return listaScripts.get(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View view = LayoutInflater.from(context).inflate(R.layout.item_script, parent, false);

        Script script = listaScripts.get(position);
        String foto = script.getFoto();

        TextView textViewNome = view.findViewById(R.id.item_script_titulo);
        TextView textViewTexto = view.findViewById(R.id.item_script_categoria);
        ImageView image = view.findViewById(R.id.item_script_img);

        //System.out.println(script.getId());

        textViewNome.setText(script.getTitulo());
        textViewTexto.setText(dao.listarCategoria(script));

        Bitmap bitmap = BitmapFactory.decodeFile(foto);
        image.setImageBitmap(bitmap);
        image.setTag(foto);


        return view;
    }

    public void atualizar(List<Script> scripts){
        listaScripts.clear();
        listaScripts.addAll(scripts);
        notifyDataSetChanged();
    }

    public void remover(Script scripts){
        listaScripts.remove(scripts);
    }
}
