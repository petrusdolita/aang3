package com.example.aang.ui;


import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.FileProvider;

import com.example.aang.BD.BancoBD;
import com.example.aang.BuildConfig;
import com.example.aang.Domain.Script;
import com.example.aang.R;
import com.example.aang.exe.Interpretador;

import java.io.File;


public class FormularioActivity extends AppCompatActivity {
    Script script2;

    private String urlFoto;
    public static final int REQUEST_CODE_CAMERA = 123;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_formulario);
        final BancoBD banco = new BancoBD(this);

        final EditText eTextTitulo= findViewById(R.id.edittext_titulo);
        final EditText eTextTexto= findViewById(R.id.edittext_texto);
        final ImageView iview = findViewById(R.id.img_form);



        //ADICIONAR IDCATEGORIA


        final Intent intent = getIntent();



        Button btnfoto = findViewById(R.id.btn_adicionarImagemForm);
        btnfoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){

                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

                urlFoto = getExternalFilesDir(null) + "/" + System.currentTimeMillis() + ".jpg";
                File file = new File(urlFoto);
                intent.putExtra(MediaStore.EXTRA_OUTPUT,
                        FileProvider.getUriForFile(
                                FormularioActivity.this,
                                BuildConfig.APPLICATION_ID + ".provider",
                                file
                        ));
                startActivityForResult(intent, 123);
            }
        });
        if(intent.hasExtra("script")){
            script2 = (Script) intent.getSerializableExtra("script");
            urlFoto = script2.getFoto();
            eTextTitulo.setText(script2.getTitulo());
            eTextTexto.setText(script2.getTexto());
            setImage(urlFoto);

        }


        Button criar = findViewById(R.id.button_form);
        criar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String titulo = eTextTitulo.getText().toString();
                String texto = eTextTexto.getText().toString();
                Script script = new Script(texto,titulo, Interpretador.identificarCategoria(texto),urlFoto);

                if(intent.hasExtra("script")&&script.getIdCategoria()!=-1) {

                    script2.setTitulo(titulo);
                    script2.setTexto(texto);
                    script2.setIdCategoria(Interpretador.identificarCategoria(texto));
                    script2.setFoto(urlFoto);
                    banco.atualizarScript(script2);


                    Context contexto = getApplicationContext();
                    String textow = "Editado com sucesso";
                    int duracao = Toast.LENGTH_SHORT;

                    Toast toast = Toast.makeText(contexto, textow, duracao);
                    toast.show();
                    finish();


                } else if(!eTextTitulo.getText().toString().isEmpty()&&!eTextTexto.getText().toString().isEmpty()&&script.getIdCategoria()!=-1) {

                    banco.adicionarScript(script);

                    Context contexto = getApplicationContext();
                    String textow = "Salvo com sucesso";
                    int duracao = Toast.LENGTH_SHORT;

                    Toast toast = Toast.makeText(contexto, textow, duracao);
                    toast.show();
                    finish();




                } else if(script.getIdCategoria()==-1){
                    Context contexto = getApplicationContext();
                    String textow = "Precisas identificar a categoria, insolente";
                    int duracao = Toast.LENGTH_SHORT;

                    Toast toast = Toast.makeText(contexto, textow, duracao);
                    toast.show();
                }else{
                    Context contexto = getApplicationContext();
                    String textow = "Precisas completar o cadastro, insolente";
                    int duracao = Toast.LENGTH_SHORT;

                    Toast toast = Toast.makeText(contexto, textow, duracao);
                    toast.show();
                }
            }

        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        setImage(urlFoto);
    }


    private void setImage(String foto) {
        ImageView imageView = findViewById(R.id.img_form);
        Bitmap bitmap = BitmapFactory.decodeFile(foto);
        imageView.setImageBitmap(bitmap);
        imageView.setTag(foto);
    }



}