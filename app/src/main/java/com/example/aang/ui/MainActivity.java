package com.example.aang.ui;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.aang.BD.BancoBD;
import com.example.aang.BD.DadosTemporarios;
import com.example.aang.Domain.Categoria;
import com.example.aang.Domain.Script;
import com.example.aang.R;
import com.example.aang.exe.BCReceiver;
import com.example.aang.exe.Executor;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.List;

import static java.lang.Thread.sleep;


public class MainActivity extends AppCompatActivity {

    private AdapterScript adapter;
    private Script script;
    private Menu menu;

    // abaixo ele chama a conexão com o banco
    protected static BancoBD dao;

    //Broadcast Receiver Dinâmico, ativar!
    private BCReceiver bcReceiver = new BCReceiver();

    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        dao = new BancoBD(this);
        adapter = new AdapterScript(dao.listarScripts(), this);

        dao.adicionarCategoria(new Categoria("IMEDIATAMENTE"));
        dao.adicionarCategoria(new Categoria("CARREGADOR"));
        dao.adicionarCategoria(new Categoria("CONECTADO"));

        ListView listView = findViewById(R.id.listview_mainactivity);
        listView.setAdapter(adapter);

        registerForContextMenu(listView);

        FloatingActionButton fbutton = findViewById(R.id.fab_ativity);
        fbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, FormularioActivity.class);
                startActivity(intent);
            }
        });
    }
    //Colocando os receivers pra serem rodados quando o app iniciar
    /*@Override
    protected void onStart() {
        super.onStart();
        IntentFilter filterConnectivity = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        registerReceiver(bcReceiver, filterConnectivity);
        IntentFilter filterPowerConnected = new IntentFilter(Intent.ACTION_POWER_CONNECTED);
        registerReceiver(bcReceiver, filterPowerConnected);
    }*/

    //Colocando os receiver para serem parados quando o app também for parado
    /*@Override
    protected void onStop() {
        super.onStop();
        unregisterReceiver(bcReceiver);
    }*/

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.menu_opcao, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item){
        if(item.getItemId() == R.id.menu_item_ver_tutorial){
            Intent intent = new Intent(MainActivity.this, TutorialActivity.class);
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo){
        super.onCreateContextMenu(menu, v, menuInfo);
        getMenuInflater().inflate(R.menu.menu_item, menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item){

        AdapterView.AdapterContextMenuInfo contextMenuInfo = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        script = (Script) adapter.getItem(contextMenuInfo.position);

        if (item.getItemId() == R.id.menu_editar){
            Intent i = new Intent (MainActivity.this, FormularioActivity.class);
            i.putExtra("script", script);
            startActivity(i);
        }
        else if(item.getItemId() == R.id.menu_apagar){
            dao.deletarScript(script);
            onResume();
        }
        //Opção executar o script
        else if(item.getItemId() == R.id.menu_executar){

            Context contexto = getApplicationContext();
            int duracao = Toast.LENGTH_SHORT;

            //Tentando colocar o Receiver pra funcionar quando o script for executado



            /*
            *   dao.adicionarCategoria(new Categoria("IMEDIATAMENTE"));
                dao.adicionarCategoria(new Categoria("CARREGADOR"));
                dao.adicionarCategoria(new Categoria("CONECTADO"));

            * */

            switch(script.getIdCategoria()){
                case 1:
                    Intent i = new Intent (this, Executor.class);
                    i.putExtra("script", script);

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        startForegroundService(i);
                    }else{
                        startService(i);
                    }
                    break;
                case 2:

                    Intent w = new Intent (this, Executor.class);
                    w.putExtra("script", script);

                    DadosTemporarios.setExecucao(true);
                    DadosTemporarios.setIntent(w);

                    IntentFilter filterPowerConnected = new IntentFilter(Intent.ACTION_POWER_CONNECTED);
                    registerReceiver(bcReceiver, filterPowerConnected);

                    break;
                case 3:

                    Intent y = new Intent (this, Executor.class);
                    y.putExtra("script", script);

                    DadosTemporarios.setExecucao(true);
                    DadosTemporarios.setIntent(y);
                    IntentFilter filterConnectivity = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
                    registerReceiver(bcReceiver, filterConnectivity);

                    break;

            }


        }

        return super.onContextItemSelected(item);
    }

    //Encerrando o Receiver quando ele parar de rodar
    @Override
    protected void onStop() {
        super.onStop();
    }

    protected void onResume(){
        super.onResume();
        adapter.atualizar(dao.listarScripts());
    }


}
