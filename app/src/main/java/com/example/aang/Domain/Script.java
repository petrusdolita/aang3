package com.example.aang.Domain;

import java.io.Serializable;

public class Script implements Serializable {
    private int id;
    private String texto;
    private String titulo;
    private int idCategoria;
    private String foto;

    public Script(String texto, String titulo, int idCategoria) {
        this.texto = texto;
        this.titulo = titulo;
        this.idCategoria = idCategoria;
    }

    public Script(int id, String texto, String titulo, int idCategoria) {
        this.id = id;
        this.texto = texto;
        this.titulo = titulo;
        this.idCategoria = idCategoria;
    }

    public Script(String texto, String titulo, int idCategoria, String foto) {
        this.texto = texto;
        this.titulo = titulo;
        this.idCategoria = idCategoria;
        this.foto = foto;
    }

    public Script(int id, String texto, String titulo, int idCategoria, String foto) {
        this.id = id;
        this.texto = texto;
        this.titulo = titulo;
        this.idCategoria = idCategoria;
        this.foto = foto;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public int getIdCategoria() {
        return idCategoria;
    }

    public void setIdCategoria(int idCategoria) {
        this.idCategoria = idCategoria;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    @Override
    public String toString() {
        return "Script{" +
                "texto='" + texto + '\'' +
                ", titulo='" + titulo + '\'' +
                ", idCategoria=" + idCategoria +
                '}';
    }
}
